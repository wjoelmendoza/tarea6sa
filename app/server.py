from flask import Flask, render_template
from base import conexion


app = Flask(__name__)


@app.route("/")
def index():
    d = conexion.Consulta()
    datos = d.get_datos()
    return render_template("index.html", datos=datos)


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, debug=True)
