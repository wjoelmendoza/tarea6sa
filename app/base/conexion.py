import mysql.connector


class Conexion(object):

    def __init__(self):
        """
        iniciando una conexion con la base de datos
        con sus respectivas credenciales
        """
        self.mydb = mysql.connector.connect(
            host='db',
            user='root',
            passwd='12345',
            database='tarea6'
        )


class Consulta(object):
    """
    Esta clase se encarga de realizar consultas
    """

    def get_datos(self):
        """
        crea una conexion, realiza una consulta
        sobre la tabla de Producto
        """
        con = Conexion()
        cons = con.mydb.cursor()
        cons.execute('Select * from Producto')

        # Abre el cursor y extrae los datos
        result = cons.fetchall()
        rst = []
        for dt in result:
            rst.append(dt)
        con.mydb.close()

        # retorna los datos de la consulta
        return rst
