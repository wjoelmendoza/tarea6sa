create database tarea6;
use tarea6;

create table Producto(
		id_producto int auto_increment,
		descripcion varchar(250) not null,
		primary key(id_producto)
);

insert into Producto ( descripcion ) values ("Producto1"),( "Producto2"), ("Producto3"),( "Producto4"),( "Producto5"),( "Producto6"),( "Producto7"),
( "Producto8"),( "Producto9"),( "Producto10");
