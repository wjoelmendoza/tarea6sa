# Tarea 6

Se creo un archivo de docker-compose para definir la infraestructura del proyecto,
en los cuales se definio una base de datos y un servidor basado en python, a 
continuación se listan los archivos utilizados:

* **docker-compose.yml:** este archivo contiene la definición de la infraestructura
* **base.sql:** este script contiene la definicion de una base de datos, una tabla 
e inserciones a la base de datos, se utiliza en conjunto con docker-compose para crea la base de datos
* **Dockerfile:** contiene la definición de una imagen, basada en python:3.7.7-alpine3.10 para
que la imagen resultante sea pequeña, y *flask* para crear el container que consume la base de datos

Se deja el video de demostracion, [link](https://youtu.be/FZHB4pI0jlU)
