FROM python:3.7.7-alpine3.10
RUN pip install flask
RUN pip install mysql-connector
RUN mkdir -p /test/app
EXPOSE 80
CMD ["python3", "/test/app/server.py"]
